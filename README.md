# C# Macro Mouse Clicker
## Usage
Middle click anywhere to record a new mouse click. 

While a macro is not running:
* clicks can be deleted by selecting them and pressing the Delete key
* position, time to wait after clickking, and mouse button can be set by editing the grid
++